<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 12.12.2017
 * Time: 18:30
 */

class SocialLink extends CachingSection
{
    /** @var string $twitterLink */
    private $twitterLink;

    /** @var  string $facebookLink */
    private $facebookLink;

    /** @var  string $githubLink */
    private $githubLink;

    /** @var  string $dribbleLink */
    private $dribbleLink;

    /** @var  string $emailLink */
    private $emailLink;

    public function __construct()
    {
        $this->setCaching(false);
        $dbc = DatabaseConnection::getInstance()->getPdo();

        foreach ($dbc->query('SELECT * FROM SocialLinks') as $row) {
            switch ($row['Name']) {
                case 'TWITTER':
                    $this->twitterLink = $row['Link'];
                    break;
                case 'FACEBOOK':
                    $this->facebookLink = $row['Link'];
                    break;
                case 'GITHUB':
                    $this->githubLink = $row['Link'];
                    break;
                case 'DRIBBLE':
                    $this->dribbleLink = $row['Link'];
                    break;
                case 'EMAIL':
                    $this->emailLink = $row['Link'];
                    break;
            }
        }
    }

    /**
     * @return string
     */
    public function getTwitterLink()
    {
        return $this->twitterLink;
    }

    /**
     * @param string $twitterLink
     */
    public function setTwitterLink($twitterLink)
    {
        $this->twitterLink = $twitterLink;
    }

    /**
     * @return string
     */
    public function getFacebookLink()
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     */
    public function setFacebookLink($facebookLink)
    {
        $this->facebookLink = $facebookLink;
    }

    /**
     * @return string
     */
    public function getGithubLink()
    {
        return $this->githubLink;
    }

    /**
     * @param string $githubLink
     */
    public function setGithubLink($githubLink)
    {
        $this->githubLink = $githubLink;
    }

    /**
     * @return string
     */
    public function getDribbleLink()
    {
        return $this->dribbleLink;
    }

    /**
     * @param string $dribbleLink
     */
    public function setDribbleLink($dribbleLink)
    {
        $this->dribbleLink = $dribbleLink;
    }

    /**
     * @return string
     */
    public function getEmailLink()
    {
        return $this->emailLink;
    }

    /**
     * @param string $emailLink
     */
    public function setEmailLink($emailLink)
    {
        $this->emailLink = $emailLink;
    }

    public function getSection() {
        $rtn = '<ul class="icons">';
        if (!is_null($this->twitterLink)) $rtn .= '<li><a href="' . $this->twitterLink . '" class="icon fa-twitter"><span class="label">Twitter</span></a></li>';
        if (!is_null($this->facebookLink)) $rtn .= '<li><a href="' . $this->facebookLink . '" class="icon fa-facebook"><span class="label">Facebook</span></a></li>';
        if (!is_null($this->githubLink)) $rtn .= '<li><a href="' . $this->githubLink . '" class="icon fa-github"><span class="label">Github</span></a></li>';
        if (!is_null($this->dribbleLink)) $rtn .= '<li><a href="' . $this->dribbleLink . '" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>';
        if (!is_null($this->emailLink)) $rtn .= '<li><a href="' . $this->emailLink . '" class="icon fa-envelope"><span class="label">Email</span></a></li>';
        return $rtn;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 08.12.2017
 * Time: 07:17
 */

class Head extends CachingSection
{
    /** @var  string $title */
    private $title;

    /** @var string $charset */
    private $charset = "utf-8";

    /** @var Meta[] $metas */
    private $metas;

    /** @var string[] $metas */
    private $scripts;

    /** @var string[] $metas */
    private $css;

    public function __construct()
    {
        $this->setCaching(true);
        $this->setUserLevel(UserLevel::Null);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * @return Meta[]
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * @param Meta[] $metas
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;
    }

    /**
     * @return string[]
     */
    public function getScripts()
    {
        return $this->scripts;
    }

    /**
     * @param string[] $scripts
     */
    public function setScripts($scripts)
    {
        $this->scripts = $scripts;
    }

    /**
     * @return string[]
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * @param string[] $css
     */
    public function setCss($css)
    {
        $this->css = $css;
    }

    private function fMeta() {
        $rtn = '';

        foreach ($this->css as &$item)
            $rtn .= '<meta name="" content="" />';

        return $rtn;
    }

    private function fCss() {
        $rtn = '';

        foreach ($this->css as &$item)
            $rtn .= '<link rel="stylesheet" href="' . $item . '" />';

        return $rtn;
    }

    private function fScripts() {
        $rtn = '';

        foreach ($this->css as &$item)
            $rtn .= '<<script src="' . $item . '"></script>';

        return $rtn;
    }

    public function getSection() {
        return
        '<title>' . $this->title . '</title>
        '. $this->charset . $this->fMeta() . $this->fScripts() . $this->fCss();
    }
}
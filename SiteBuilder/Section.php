<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 06.12.2017
 * Time: 19:44
 */

class Section extends CachingSection
{
    /** @var  string $id */
    private $id;

    /** @var  string $class */
    private $class;

    /** @var  string $innerHTML */
    private $innerHTML;

    public function __construct($id, $class, $innerHTML)
    {
        $this->id = $id;
        $this->class = $class;
        $this->innerHTML = $innerHTML;
        $this->setCaching(false);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getInnerHTML()
    {
        return $this->innerHTML;
    }

    /**
     * @param string $innerHTML
     */
    public function setInnerHTML($innerHTML)
    {
        $this->innerHTML = $innerHTML;
    }

    public function getSection() {
        return
        '<section id="' . $this->id . '" class="' . $this->class . '">
        ' . $this->innerHTML . '
        </section>';
    }
}
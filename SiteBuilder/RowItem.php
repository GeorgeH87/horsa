<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 01.12.2017
 * Time: 07:43
 */



class RowItem extends CachingSection {
    private $imageFile;
    private $text;

    public function __construct($imageFile, $text)
    {
        $this->imageFile = $imageFile;
        $this->text = $text;
        $this->setCaching(false);
    }

    /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    public function getSection() {
        return
            '<article class="item">
                <a href="#" class="image fit"><img src="' . $this->imageFile . '" alt="" /></a>
                <header>
                    <h3>' . $this->text . '</h3>
                </header>
            </article>';
    }
}

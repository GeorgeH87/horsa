<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 06.12.2017
 * Time: 19:34
 */

class ImageView extends CachingSection
{
    /** @var RowItem[] $rowItems */
    private $rowItems;

    /** @var int $rows */
    private $rows;

    public function __construct()
    {
        $this->setCaching(false);
        $this->setUserLevel(UserLevel::Null);
    }

    /**
     * @param $items
     * @return string
     */
    private function getRow($items) {
        $rtn = '<div class="4u 12u$(mobile)">';

        /** @var RowItem $item */
        foreach ($items as $item) {
            $rtn .= $item->getSection();
        }

        $rtn .= '</div>';
        return $rtn;
    }

    public function getSection() {
        $rtn = '<div class="row">';

        if ($this->rows % 3 == 0) {

        } else {

        }

        $rtn .= '</div>';
        return $rtn;
    }
}
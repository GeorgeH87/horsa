<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 12.12.2017
 * Time: 18:40
 */

abstract class CachingSection
{
    /** @var int $userLevel */
    private $userLevel = UserLevel::Null;

    /** @var bool $caching */
    private $caching = true;

    /** @var  string $cachedData */
    private $cachedData;

    abstract function getSection();

    function refresh() {
        $this->cachedData = $this->getSection();
        return $this->cachedData;
    }

    function get() {
        /** @var Session $horsaSession */
        $horsaSession = $_SESSION("horsaSession");

        if ($horsaSession->getUser() >= $this->userLevel) {
            if ($this->caching) {
                if (is_null($this->cachedData))
                    $this->cachedData = $this->getSection();
                return $this->cachedData;
            } else {
                return $this->getSection();
            }
        } else {
             return "";
        }
    }

    /**
     * @return int
     */
    public function getUserLevel()
    {
        return $this->userLevel;
    }

    /**
     * @param int $userLevel
     */
    public function setUserLevel($userLevel)
    {
        $this->userLevel = $userLevel;
    }

    /**
     * @return bool
     */
    public function isCaching()
    {
        return $this->caching;
    }

    /**
     * @param bool $caching
     */
    public function setCaching($caching)
    {
        $this->caching = $caching;
    }
}
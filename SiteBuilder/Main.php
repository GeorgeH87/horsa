<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 08.12.2017
 * Time: 07:16
 */

class Main extends CachingSection
{
    /** @var Head $head*/
    private $head;

    /** @var Body $body */
    private $body;

    function __construct()
    {
        $this->setCaching(false);
        $this->setUserLevel(UserLevel::Null);
    }

    function getSection()
    {

    }

    /**
     * @return Head
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * @param Head $head
     */
    public function setHead($head)
    {
        $this->head = $head;
    }
}
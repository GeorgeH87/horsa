<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 30.11.2017
 * Time: 22:30
 */

class MenuEntry extends CachingSection
{
    /** @var  string name */
    private $name;

    /** @var string $title */
    private $title;

    /** @var  string $description */
    private $description;

    /** @var  string $innerHTML */
    private $innerHTML;

    public function __construct()
    {
        $this->setCaching(true);
        $this->setUserLevel(UserLevel::Null);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->name = StandardFunctions::clean($title);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInnerHTML()
    {
        return $this->innerHTML;
    }

    /**
     * @param string $innerHTML
     */
    public function setInnerHTML($innerHTML)
    {
        $this->innerHTML = $innerHTML;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getMenuEntry()
    {
        return '<li><a href="#' . $this->name . '" id="' . $this->name . '" class="skel-layers-ignoreHref"><span class="icon fa-th">' . $this->title . '</span></a></li>';
    }

    public function getSection() {
        return '
        <section id=\"'. $this->name .'\" class=\"another-section\">
						<div class=\"container\">

							<header>
								<h2>'. $this->title .'</h2>
							</header>

							<p>' . $this->innerHTML . '</p>

							<div class=\"row\">
								<div class=\"4u 12u$(mobile)\">
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic02.jpg\" alt=\"\" /></a>
										<header>
											<h3>Ipsum Feugiat</h3>
										</header>
									</article>
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic03.jpg\" alt=\"\" /></a>
										<header>
											<h3>Rhoncus Semper</h3>
										</header>
									</article>
								</div>
								<div class=\"4u 12u$(mobile)\">
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic04.jpg\" alt=\"\" /></a>
										<header>
											<h3>Magna Nullam</h3>
										</header>
									</article>
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic05.jpg\" alt=\"\" /></a>
										<header>
											<h3>Natoque Vitae</h3>
										</header>
									</article>
								</div>
								<div class=\"4u$ 12u$(mobile)\">
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic06.jpg\" alt=\"\" /></a>
										<header>
											<h3>Dolor Penatibus</h3>
										</header>
									</article>
									<article class=\"item\">
										<a href=\"#\" class=\"image fit\"><img src=\"images/pic07.jpg\" alt=\"\" /></a>
										<header>
											<h3>Orci Convallis</h3>
										</header>
									</article>
								</div>
							</div>

						</div>
					</section>
        ';
    }
}
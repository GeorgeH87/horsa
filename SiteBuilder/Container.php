<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 06.12.2017
 * Time: 19:36
 */

class Container extends CachingSection
{
    private $title;
    private $image;
    private $innerHTML;

    public function __construct($title, $image, $innerHTML)
    {
        $this->title = $title;
        $this->image = $image;
        $this->innerHTML = $innerHTML;
        $this->setCaching(true);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getInnerHTML()
    {
        return $this->innerHTML;
    }

    /**
     * @param mixed $innerHTML
     */
    public function setInnerHTML($innerHTML)
    {
        $this->innerHTML = $innerHTML;
    }

    public function getSection() {
        $rtn = '<div class="container"><header><h2>' . $this->title . '</h2></header>';

        if (!is_null($this->image))
            $rtn .= '<a href="#" class="image featured"><img src="' . $this->image . '" alt="" /></a>';

        $rtn .= '<p>' . $this->innerHTML . '</p></div>';
        return $rtn;
    }
}
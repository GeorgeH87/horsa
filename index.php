<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 14.12.2017
 * Time: 05:57
 */

include 'DatabaseConnection.php';
include 'Main.php';
include 'StandardFunctions.php';

include 'Data/Activity.php';
include 'Data/Message.php';
include 'Data/Blacklist.php';

Blacklist::check(); //Auf Blacklist prüfen
Activity::setSession(); //write horsa session data into php session

?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php print(Main::getProperty("Title")) ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
</head>
    <body>
        <?php
        //parse_str($_SERVER['QUERY_STRING'], $arr);
        include 'Sections/Header.php';
        include 'Sections/Footer.php';
        include 'Sections/Scripts.php';
        ?>
    </body>
</html>

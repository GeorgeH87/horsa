<?php
/**
 * Created by PhpStorm.
 * User: georg
 * Date: 29.11.2017
 * Time: 15:21
 */

class StandardFunctions
{
    public static function generateRandomString($length = 10, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param string $text
     * @param string $mixer
     * @return string
     */
    public static function mixString($text, $mixer) {
        /** @var string $slft */
        $txf = $mixer;

        /** @var string $rtn */
        $rtn = "";

        for ($i = 0; $i < strlen($text); $i++) {
            if ($i >= strlen($txf))
                $txf .= $mixer;

            $rtn .= $text[$i] . $txf[$i];
        }

        return rtn;
    }

    public static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * @param string $string
     * @param string[] $array
     * @param bool $cleanIt
     * @return string
     */
    public static function doString($string, $array = [], $cleanIt = true) {
        if (count($array) == 0)
            return $string;

        if ($cleanIt)
            $fun = self::clean($array[0]);
        else
            $fun = $array[0];

        $strs =  preg_replace('/\?/', $fun, $string, 1);
        $narray = array_slice($array, 1);

        $strs = self::doString($strs, $narray);

        return $strs;
    }

    public static function addToBlacklist() {
        DatabaseConnection::exec("INSERT INTO horsa.Blacklist (address, activity, active) VALUES ('?', NOW() , '1')", [$_SERVER["REMOTE_ADDR"]]);
    }
}
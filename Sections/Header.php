<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 19.12.2017
 * Time: 18:32
 */

?>
<!-- Header -->
<div id="header">
    <div class="top">
        <!-- Logo -->
        <div id="logo">
            <span class="image avatar48"><img src="<?php print(Main::getProperty("Avatar")) ?>" alt="" /></span>
            <h1 id="title"><?php print(Main::getProperty("Title")) ?></h1>
            <p><?php print(Main::getProperty("TitleSub", false)) ?></p>
        </div>

        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Intro</span></a></li>
                <li><a href="#portfolio" id="portfolio-link" class="skel-layers-ignoreHref"><span class="icon fa-th">Portfolio</span></a></li>
                <li><a href="#about" id="about-link" class="skel-layers-ignoreHref"><span class="icon fa-user">About Me</span></a></li>
                <li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-envelope">Contact</span></a></li>
            </ul>
        </nav>

    </div>

    <div class="bottom">
        <!-- Social Icons -->
        <ul class="icons">
            <?php
            $dbc = DatabaseConnection::getInstance()->getPdo();

            foreach ($dbc->query('SELECT * FROM SocialLinks') as $row) {
                switch ($row['Name']) {
                    case 'TWITTER':
                        ?><li><a href="<?php print($row['Link'])?>" class="icon fa-twitter"><span class="label">Twitter</span></a></li><?php
                        break;
                    case 'FACEBOOK':
                        ?><li><a href="<?php print($row['Link'])?>" class="icon fa-facebook"><span class="label">Facebook</span></a></li><?php
                        break;
                    case 'GITHUB':
                        ?><li><a href="<?php print($row['Link'])?>" class="icon fa-github"><span class="label">Github</span></a></li><?php
                        break;
                    case 'DRIBBLE':
                        ?><li><a href="<?php print($row['Link'])?>" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li><?php
                        break;
                    case 'EMAIL':
                        ?><li><a href="<?php print($row['Link'])?>" class="icon fa-envelope"><span class="label">Email</span></a></li><?php
                        break;
                }
            }
            ?>
        </ul>
    </div>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: georg
 * Date: 30.11.2017
 * Time: 15:00
 */

class Main
{
    /** @var Holder $holder */
    private static $holder = NULL;

    /** @var SocialLink  */
    private static $socialLinks;

    /**
     * @return Holder
     */
    public static function getHolder()
    {
        if (is_null(self::$holder))
            self::$holder = new Holder();

        return self::$holder;
    }

    public function query($statement) {
        $dbc = DatabaseConnection::getInstance();
        return $dbc->getPdo()->query($statement);
    }

    public function queryFirst($statement) {
        foreach (self::query($statement) as $row) {
            return($row);
        }
        return null;
    }

    public static function getProperty($name, $standardValue = null) {
        $cn = StandardFunctions::clean($name);
        $row = DatabaseConnection::queryFirst("SELECT * FROM horsa.Properties WHERE name = '?'", [$cn]);

        if (is_null($row)) {
            if (is_null($standardValue))
                return null;
            DatabaseConnection::exec("INSERT INTO horsa.Properties (name, value) VALUES ('?', '?')", [$name, base64_encode($standardValue)]);
            return $standardValue;
        } else {
            return base64_decode($row['value']);
        }
    }

    public static function setProperty($name, $value, $userLevel) {
        $cn = StandardFunctions::clean($name);
        if (User::getActiveUser()['level'] < $userLevel) {
            //TODO: User got not right to change value
            throw new Exception("User got not right to change value");
        }

        $encodedValue = base64_encode($value);
        DatabaseConnection::exec("UPDATE horsa.Properties SET value = '?' WHERE name = '?'", [$encodedValue, $name]);
    }

    public static function getSession() {
        $id = $_SESSION['HorsaSession'];
        if (is_null($id)) {
            session_start();
        } else {

        }
    }

    public static function getMenu() {

    }

    private static $num = 0;

    public static  function getNum() {
            return ++self::$num;
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 14.12.2017
 * Time: 05:31
 */

class DatabaseConnection
{
    /** @var PDO $pdo */
    private $pdo;

    private $host;
    private $dbName;
    private $user;
    private $pass;

    /** @var DatabaseConnection $instance */
    private static $instance;

    public function connect() {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $this->pdo = new PDO($dsn, $this->user, $this->pass, array(
            PDO::ATTR_PERSISTENT => true
        ));
    }

    public function __construct($host, $dbName, $user, $pass)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->pass = $pass;
    }

    public static function getInstance() {
        if (is_null(DatabaseConnection::$instance)) {
            DatabaseConnection::$instance = new DatabaseConnection("localhost", "horsa", "horsaUser", "P6UIsGfBV2uwOVlK");
            DatabaseConnection::$instance->connect();
        }
        return DatabaseConnection::$instance;
    }

    public static function query($string, $array = []) {
        $ss = StandardFunctions::doString($string, $array);
        return self::getInstance()->getPdo()->query($ss);
    }

    public static function queryFirst($string, $array = [], $exception = false) {
        $ss = StandardFunctions::doString($string, $array);
        $ss .= " LIMIT 1";

        foreach (self::getInstance()->getPdo()->query($ss) as $row) {
            return $row;
        }

        if ($exception)
            throw new Exception("Couldn't load first database entry: " . $string);

        return null;
    }

    public static function exec($string, $array = []) {
        $ss = StandardFunctions::doString($string, $array);
        self::getInstance()->getPdo()->exec($ss);
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}
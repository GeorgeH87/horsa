#<?php
/**
 * Created by PhpStorm.
 * User: georg
 * Date: 30.11.2017
 * Time: 08:32
 */

class Timer
{
    /** @var TimerInterface[] $list */
    private $list = array();

    public function __construct()
    {

    }

    /**
     * @param TimerInterface $entry
     */
    public function addEntry(&$entry)
    {
        array_push($this->list, $entry);
        $entry->setParent($this);
    }

    public function removeEntry(&$entry) {
        if (($key = array_search($entry, $this->list)) !== false) {
            unset($this->list[$key]);
        }
    }

    /**
     * @return TimerInterface[]
     */
    public function getList()
    {
        return $this->list;
    }

    public function work() {
        $remove = array();

        foreach ($this->list as &$entry) {
            if (time() - $entry->getStartTime() >= $entry->getTimer()) {
                if ($entry->doTimer()) {
                    array_push($remove, $entry);
                } else {
                    $entry->resetStartTime();
                }
            }
        }

        foreach ($remove as &$entry)
            $this->removeEntry($entry);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: georg
 * Date: 30.11.2017
 * Time: 08:24
 */

abstract class TimerInterface
{
    /** @var  Timer $parent */
    private $parent;

    /** @var  int $startTime */
    private $startTime;

    /** @var  int $time */
    private $time;

    public function __construct($time)
    {
        $this->startTime = time();
        $this->time = time;
    }

    public function resetStartTime() {
        $this->startTime = time();
    }

    /**
     * @param int $time
     */
    public function setTime($time) {
        $this->time = $time;
    }

    /**
     * @return int
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * @return int
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * @return bool
     */
    public abstract function doTimer();

    /**
     * @return Timer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Timer $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
}
-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 22. Okt 2017 um 09:24
-- Server Version: 5.5.58-0+deb8u1
-- PHP-Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `horsa`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Activities`
--

CREATE TABLE IF NOT EXISTS `Activities` (
`id` int(11) NOT NULL,
  `pageName` text NOT NULL,
  `session` varchar(30) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `GuestUser`
--

CREATE TABLE IF NOT EXISTS `GuestUser` (
`id` int(11) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `telephoneNumber` text NOT NULL,
  `address` text NOT NULL,
  `postcode` text NOT NULL,
  `city` text NOT NULL,
  `country` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `HostEntries`
--

CREATE TABLE IF NOT EXISTS `HostEntries` (
`id` int(11) NOT NULL,
  `address` text NOT NULL,
  `firstAccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastAccess` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Offer`
--

CREATE TABLE IF NOT EXISTS `Offer` (
`id` int(11) NOT NULL,
  `guestUser` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Properties`
--

CREATE TABLE IF NOT EXISTS `Properties` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `PublicOffers`
--

CREATE TABLE IF NOT EXISTS `PublicOffers` (
`id` int(11) NOT NULL,
  `releaseDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `offer` int(11) NOT NULL,
  `moderator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Sessions`
--

CREATE TABLE IF NOT EXISTS `Sessions` (
  `id` varchar(30) NOT NULL,
  `firstActivity` int(11) NOT NULL,
  `lastActivity` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `guestUser` int(11) NOT NULL,
  `host` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `name` varchar(24) NOT NULL,
  `passwordHash` varchar(64) NOT NULL,
  `firstLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blocked` tinyint(1) NOT NULL,
  `blockedReason` text NOT NULL,
  `failedCounter` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Activities`
--
ALTER TABLE `Activities`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `GuestUser`
--
ALTER TABLE `GuestUser`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `HostEntries`
--
ALTER TABLE `HostEntries`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `Offer`
--
ALTER TABLE `Offer`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `PublicOffers`
--
ALTER TABLE `PublicOffers`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `Sessions`
--
ALTER TABLE `Sessions`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `Users`
--
ALTER TABLE `Users`
 ADD PRIMARY KEY (`name`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Activities`
--
ALTER TABLE `Activities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `GuestUser`
--
ALTER TABLE `GuestUser`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `HostEntries`
--
ALTER TABLE `HostEntries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `Offer`
--
ALTER TABLE `Offer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `PublicOffers`
--
ALTER TABLE `PublicOffers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

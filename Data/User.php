<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 05.01.2018
 * Time: 07:47
 */

class User
{
    private static $activeUserIsSet = false;
    private static $userSessionIsSet = false;

    private static $activeUser = null;
    private static $userSession = null;

    public static function logout() {
        if (is_null(self::getActiveUser())) {
            //TODO: can not logut a not logged in user
            throw new Exception("can not logut a not logged in user");
        }

        DatabaseConnection::exec("UPDATE horsa.UserSessions SET Active = '0', Logout = NOW() WHERE Session = '?'", [Session::getSession()['id']]);
    }

    public static function getActiveUser() {
        if (self::$activeUserIsSet)
            return self::$activeUser;

        self::$activeUser = DatabaseConnection::queryFirst("SELECT * FROM horsa.Users WHERE name = '?'", self::getUserSession()['User']);
        self::$activeUserIsSet = true;

        return self::$activeUser;
    }

    public static function getUserSession() {
        if (self::$userSessionIsSet)
            return self::$userSession;

        self::$userSession = DatabaseConnection::queryFirst("SELECT * FROM horsa.UserSessions WHERE Session = '?'", [Session::getSession()['id']]);
        self::$userSessionIsSet = true;

        return self::$userSession;
    }

    public static function getUser($userName) {
        return DatabaseConnection::queryFirst("SELECT * FROM horsa.Users WHERE name = '?'", [$userName]);
    }

    public static function prepareLogin($userName) {
        $row = self::getUser($userName);
        if (is_null($row))
            return null;

        $newSalt = StandardFunctions::generateRandomString(32);
        DatabaseConnection::exec("INSERT INTO horsa.UserSessions (Session, User, Salt, Status, Login, Active) VALUES ('?', '?', '?', '?', NOW(), '0')", [$_SESSION['SESSION']['id'], $row['id'], $newSalt, "PREPARE"]);

        return $newSalt;
    }

    /**
     * @param $userName User input
     * @param $passwordHash User input
     * @return string[] UserSession returns an UserSession SQL Row or a null pointer
     */
    public static function login($userName, $passwordHash) {
        $userRow = self::getUser($userName);
        $sessionRow = self::getUserSession();

        if (is_null($userRow)) {
            //TODO: Logging user does not exist
            return null;
        }

        if (is_null($sessionRow)) {
            //TODO: Logging session does not exist
            return null;
        }

        $mixedServer = StandardFunctions::mixString($userRow['passwordHash'], $sessionRow['Salt']);

        if (hash('sha256', $mixedServer) != $passwordHash)
        {
            //TODO: LOGIN failed
            return null;
        }

        //User is logged in
        DatabaseConnection::exec("UPDATE horsa.UserSessions SET Status = 'REGISTERED', Login = NOW(), Active = '1' WHERE User = '?'", [$userRow['id']]);
        return DatabaseConnection::queryFirst("SELECT * FROM horsa.UserSessions WHERE User = '?'", [$userRow['id']]);
    }
}
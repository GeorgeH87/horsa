<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 04.01.2018
 * Time: 19:27
 */

class Activity
{
    public static function addActivity($pageName) {
        if (is_null(Session::getSession()))
            throw new Exception("Session is not set");
        DatabaseConnection::exec("INSERT INTO horsa.Activities (pageName, session, time) VALUES ('?', '?', now())", [$pageName, Session::getSession()['id']]);
    }
}
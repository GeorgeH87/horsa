<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 07.01.2018
 * Time: 13:57
 */

class Session
{
    private static $sessionIsSet = false;
    private static $session = null;

    public static function getSession() {
        if (self::$sessionIsSet)
            return self::$session;

        self::$session = self::setSession();
        self::$sessionIsSet = true;

        return self::$session;
    }

    private static function setSession() {
        session_start();

        if (session_status() == PHP_SESSION_ACTIVE) {
            if (isset($_SESSION['ID'])) {
                $row = DatabaseConnection::queryFirst("SELECT * FROM horsa.Sessions WHERE phpSession = '?'", [$_SESSION['ID']]);

                if (is_null($row)) {
                    return self::createSession();
                }

                if (isset($_COOKIE['horsa'])) {
                    if ($row['cookieSession'] != $_COOKIE['horsa']) {
                        throw new Exception("invalid/different session IDs");
                    }
                } else {
                    setcookie("horsa", $row['cookieSession']);
                }
                return $row;
            } else {
                return self::checkCookie();
            }
        } else {
            return self::checkCookie();
        }
    }

    private static function createSession() {
        $phpSession = StandardFunctions::generateRandomString(30);
        $cookieSession = StandardFunctions::generateRandomString(30);
        $_SESSION['ID'] = $phpSession;
        setcookie("horsa", $cookieSession);
        DatabaseConnection::exec("INSERT INTO horsa.Sessions (phpSession, cookieSession, hostAddress) VALUES ('?', '?', '?')", [$phpSession, $cookieSession, $_SERVER['REMOTE_ADDR']]);
        return DatabaseConnection::queryFirst("SELECT * FROM horsa.Sessions WHERE phpSession = '?'", [$_SESSION['ID']]);
    }

    private static function checkCookie() {
        if (isset($_COOKIE['horsa'])) {
            $row = DatabaseConnection::queryFirst("SELECT * FROM horsa.Sessions WHERE cookieSession = '?'", [$_COOKIE['horsa']]);
            $_SESSION['ID'] = $row['phpSession'];
            return $row;
        } else {
            return self::createSession();
        }
    }
}
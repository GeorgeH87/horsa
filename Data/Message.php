<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 04.01.2018
 * Time: 21:39
 */

class Message
{
    public static function add($level, $sender, $message) {
        DatabaseConnection::exec("INSERT INTO horsa.Messages (level, sender, message, phpSession, cookieSession) VALUES ('?', '?', '?', '?', '?')", [$level, $sender, $message, $_SESSION['ID'], $_COOKIE['horsaID']]);
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: georg
 * Date: 29.12.2017
 * Time: 14:30
 */

class Blacklist {
    private static $blacklistEntryIsSet = false;
    private static $blacklistEntry = null;

    public static function getBlacklistEntry() {
        if (self::$blacklistEntryIsSet)
            return self::$blacklistEntry;

        self::$blacklistEntry = DatabaseConnection::queryFirst("SELECT * FROM horsa.Blacklist WHERE address = '?'", [$_SERVER["REMOTE_ADDR"]]);
        self::$blacklistEntryIsSet = true;
        return self::$blacklistEntry;
    }

    public static function hitCounter() {
        if (is_null(self::getBlacklistEntry())) {
            DatabaseConnection::exec("INSERT INTO horsa.Blacklist (address, activity, active, counter) VALUES ('?', NOW(), '0', '1')", [$_SERVER["REMOTE_ADDR"]]);
            self::$blacklistEntryIsSet = false;
            self::getBlacklistEntry();
        } else {
            DatabaseConnection::exec("UPDATE horsa.Blacklist SET counter = (counter + 1), activity = NOW() WHERE address='?'", [$_SERVER["REMOTE_ADDR"]]);
        }
    }

    public static function check() {
        $entry = self::getBlacklistEntry();

        if (is_null($entry))
            return;

        $zeit = strtotime($entry['activity']);
        $maxBlack = Main::getProperty("MAXBLACKLIST", 120); //2 Minuten


        if ((time() - $zeit) > $maxBlack) {
            DatabaseConnection::exec("UPDATE horsa.Blacklist SET active = '0', counter = '0' WHERE address = '?'", [$_SERVER['REMOTE_ADDR']]);
            return;
        }

        if ($entry['active'] == '1')
            throw new Exception("Already on Blacklist!");

        $maxCounter = Main::getProperty("MAXHITCOUNTER", 6);
        if ($entry['counter'] >= $maxCounter) {
            DatabaseConnection::exec("UPDATE horsa.Blacklist SET active = '1' WHERE address = '?'", [$_SERVER['REMOTE_ADDR']]);
            throw new Exception("Ooooo that is bad!");
        }
    }
}
